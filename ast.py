#Paul Camardo
#e ::= true | false | not e1 | e1 and e2 | e1 or e2

# size -- compute the size of an expression
# height -- compute the height of an expression
# same -- Return true if two expressions are identical
# value -- compute the value of an expression
# step -- Return an expression representing a single step of evaluation
# reduce -- Calls step repeatedly until the expression is non-reducible

class Expr:

class BoolExpr(Expr):
	def __init__(self,val)
		assert val == True or val == False
		self.value = val

class NotExpr(Expr):
	def __init__(self,e)
		self.value = val
		assert isinstance(e,Expr)
		self.expr = e

class AndExpr(Expr):
	def __init__(self,e1,e2)
		assert isinstance(e1,Expr,e2)
		self.lhs = e1
		self.rhs = e2

class OrExpr(Expr):
	def __init__(self,e1,e2)
		assert isinstance(e1,Expr,e2)
		self.lhs = e1
		self.rhs = e2


class IdExpr(Expr):
  	def __init__(self, id):
    	self.id = id
    	self.ref = None

  	def __str__(self):
    	return self.id

class VarDecl:
  
	def __init__(self, id):
		self.id = id

	def __str__(self):
		return self.id

class AbsExpr(Expr):
  	def __init__(self, var, e1):
    if type(var) is str:
      	self.var = VarDecl(var)
    else:
     	self.var = var
    self.expr = e1

  	def __str__(self):
    	return f"\\{self.var}.{self.expr}"

class AppExpr(Expr):
  	def __init__(self, lhs, rhs):
    	self.lhs = lhs
    	self.rhs = rhs

  	def __str__(self):
    	return f"({self.lhs} {self.rhs})"


