#Paul Camardo
# Theory of Programming Languages

from lang import *
import copy
clone = copy.deepcopy



class Closures:
	def __init__(self, abs, env):
		assert isinstance(abs, LambdaAbstraction_unary_expr)
		assert type(env) is dict

		self.abs = abs
		self.env = clone(env)

	def __str__(self):
		env = ", ".join(map(str, self.env))
		return f'<closures|abs:{self.abs}|env:[{env}]>'

	def __repr__(self):
		env = ", ".join(map(repr, self.env))
		return f'<closures|{repr(self.abs)}|env:[{env}]|{address(self)}>'


def evaluate(e, store = {}):
	assert isinstance(e, Expr)
	assert type(store) is dict

	if isinstance(e, Literal_nullary_expr):
		return e.value

	special_table = {
		Id_expr: eval_id,
		Abstraction_expr: eval_abstraction,
		Lambda_expr: eval_lambda,
		Application_expr: eval_application,
		Call_expr: eval_call}
	if type(e) in special_table:
		return special_table[type(e)](e, store)

	op = type(e).get_operation()
	if isinstance(e, Unary_expr):
		return op(evaluate(e.expr, store))
	if isinstance(e, Binary_expr):
		return op(evaluate(e.lhs, store), evaluate(e.rhs, store))
	if isinstance(e, Ternary_expr):
		x = op(evaluate(e.e1, store), e.e2, e.e3)
		return evaluate(x, store)
	assert False

def eval_id(e, store):
	if e.ref not in store:
		raise Exception(f"{e.ref} is not there")

	return store[e.ref]

def eval_abstraction(e, store):

	return Closures(e, store)

def eval_lambda(e, store):
	
	return Closures(e, store)

def eval_application(e, store):
	
	c = evaluate(e.lhs, store)

	if type(c) is not Closures:
		raise Exception("can't apply non-closures to an argument")

	v = evaluate(e.rhs, store)

	env = clone(c.env)
	env[c.abs.var] = v


	return evaluate(c.abs.expr, env)

def eval_call(e, store):
	c = evaluate(e.fn, store)

	if type(c) is not Closures:
		raise Exception("can't apply non-closures to an argument")

	args = []
	for a in e.args:
		args += [evaluate(a, store)]

	env = clone(c.env)
	for i in range(len(args)):
		env[c.abs.vars[i]] = args[i]

	return evaluate(c.abs.expr, env)
