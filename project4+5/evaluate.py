#Paul Camardo
# Theory of Programming Languages

from lang import *
from lookup import *

import copy
clone = copy.deepcopy

#value of lamda abstraction
class Closure:
  def __init__(self, abstraction, environment):
    self.abstraction = abstraction
    self.environment = clone(environment)

def eval_bool(e, store):
  return e.val

def eval_not(e, store):
  return not evaluate(e.expr, store)

def eval_and(e, store):
  return evaluate(e.lhs, store) and evaluate(e.rhs, store)

def eval_or(e, store):
  return evaluate(e.lhs, store) or evaluate(e.rhs, store)

def eval_cond(e, store):
  if evaluate(e.cond):
    return evaluate(e.true);
  else:
    return evaluate(e.false);

def eval_id(e, store):
  return store[e.ref]

def eval_app(e, store):
  c = evaluate(e.lhs, store)

  if type(c) is not Closure:
    raise Exception("Can't apply a non-closure to an argument")

  v = evaluate(e.rhs, store)

  return evaluate(c.abstraction.expr, c.environment + {c.abstraction.var: v})

def eval_abstraction(e, store):
  return Closure(e, store)

def eval_lambda(e, store):
  return Closure(e, store)

def eval_call(e, store):
  c = evaluate(e.fn, store) 
  if type(c) is not Closure:
    raise Exception("Can't apply a non-closure to an argument")
  args = []
  for a in e.args:
    args += [evaluate(a, store)]
  environment = clone(c.environment)
  for i in range(len(args)):
    environment[c.abstraction.vars[i]] = args[i]
  return evaluate(c.abstraction.expr, environment)

 def eval_call(e : Expr, stack : dict, heap : list):
  if type(c) is not Closure:
    raise Exception("cannot apply a non-closure to an argument")
  args = []
  for a in e.args:
    args += [evaluate(a, stack, heap)]

  env = clone(c.env)
  for i in range(len(args)):
    env[c.abs.vars[i]] = args[i]

  return evaluate(c.abs.expr, env, heap)

def eval_new(e : Expr, stack : dict, heap : list):
  v1 = evaluate(e.expr, stack, heap)
  l1 = Location(len(heap))
  heap += [v1]
  return l1

def eval_deref(e : Expr, stack : dict, heap : list):
  l1 = evaluate(e.expr, stack, heap)
  if type(l1) is not Location:
    raise Exception("invalid reference")
  return heap[l1.index]

def eval_assign(e : Expr, stack : dict, heap : list):
  v2 = evaluate(e.rhs, stack, heap)
  l1 = evaluate(e.lhs, stack, heap)
  if type(l1) is not Location:
    raise Exception("invalid reference")
  heap[l1.index] = v2

def eval_tuple(e : Expr, stack : dict, heap : list):
  vs = []
  for x in e.elems:
    vs += [evaluate(x, stack, heap)]
  return Tuple(vs)

def eval_proj(e : Expr, stack : dict, heap : list):
  # FIXME: Document semantics.
  v1 = evaluate(e.obj, stack, heap)
  return v1.values[e.index]

def eval_record(e : Expr, stack : dict, heap : list):
  # FIXME: Document semantics.
  fs = []
  for f in e.fields:
    fs += [Field(f.id, evaluate(f.value, stack, heap))]
  return Record(fs)

def eval_member(e : Expr, stack : dict, heap : list):
  # FIXME: Document semantics.
  v1 = evaluate(e.obj, stack, heap)
  return v1.select[e.id]

def eval_variant(e : Expr, stack : dict, heap : list):
  v1 = evaluate(e.field.value)
  return Variant(e.field.id, v1)

def eval_case(e : Expr, stack : dict, heap : list):
  v1 = evaluate(e.expr, stack, heap)

  # Search for the corresponding label.
  #
  # This could be more efficient if we produced a mapping from
  # labels to cases.
  case = None
  for c in e.cases:
    if c.id == v1.tag:
      case = c
      break
  assert case != None

  # Execute the case as if calling a function.
  env = clone(stack)
  env[c.var] = v1.value
  return evaluate(c.expr, env, heap)



def evaluate(e, store = {}):
  if type(e) is BoolExpr:
    return eval_bool(e, store)

  if type(e) is NotExpr:
    return eval_not(e, store)

  if type(e) is AndExpr:
    return eval_and(e, store)

  if type(e) is OrExpr:
    return eval_or(e, store)

  if type(e) is IdExpr:
    return eval_id(e, store)

  if type(e) is AbstractionExpr:
    return eval_abstraction(e, store)

  if type(e) is AppExpr:
    return eval_app(e, store)

  if type(e) is LambdaExpr:
    return eval_lambda(e, store)

  if type(e) is CallExpr:
    return eval_call(e, store)

  if type(e) is NewExpr:
    return eval_new(e, stack, heap)

  if type(e) is DerefExpr:
    return eval_deref(e, stack, heap)

  if type(e) is AssignExpr:
    return eval_assign(e, stack, heap)

  if type(e) is TupleExpr:
    return eval_tuple(e, stack, heap)

  if type(e) is ProjExpr:
    return eval_proj(e, stack, heap)

  if type(e) is RecordExpr:
    return eval_record(e, stack, heap)

  if type(e) is MemberExpr:
    return eval_member(e, stack, heap)

  if type(e) is VariantExpr:
    return eval_variant(e, stack, heap)

  if type(e) is CaseExpr:
    return eval_case(e, stack, heap)

