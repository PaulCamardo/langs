#Paul Camardo
# Theory of Programming Languages

class Type:
  pass

class BoolType(Type):
  def __str__(self):
    return "Bool"

class IntType(Type):
  def __str__(self):
    return "Int"

class ArrowType(Type):
  def __init__(self, type1, type2):
    self.parm = type1
    self.ret = type2
  
  def __str__(self):
    return f"({self.lhs} -> {self.rhs}"

class FnType(Type):
 def __init__(self, parms, ret):
    self.parms = parms
    self.ret = ret

boolType = BoolType()

intType = IntType()


class Expr:
  pass

class BoolExpr(Expr):
  def __init__(self, val):
    assert val == True or val == False
    self.val = val

  def __str__(self):
    return "true" if self.val else "false"

class NotExpr(Expr):
  def __init__(self, e):
    assert isinstance(e,Expr)
    self.expr = expr(e)

  def __str__(self):
    return f"(not {self.expr})"

class AndExpr(Expr):
  def __init__(self, e1, e2):
    assert isinstance(e1,Expr,e2)
    self.lhs = expr(e1)
    self.rhs = expr(e2)

  def __str__(self):
    return f"({self.lhs} and {self.rhs})"

class OrExpr(Expr):
  def __init__(self, e1, e2):
    assert isinstance(e1,Expr,e2)
    self.lhs = expr(e1)
    self.rhs = expr(e2)

  def __str__(self):
    return f"({self.lhs} or {self.rhs})"

class IfExpr(Expr):

  def __init__(self, e1, e2, e3):
    self.cond = expr(e1)
    self.true = expr(e2)
    self.false = expr(e3)

  def __str__(self):
    return f"(if {self.cond} then {self.true} else {self.false})"

class IdExpr(Expr):
  def __init__(self, x):
    if type(x) is str:
      self.id = x
      self.ref = None 
    elif type(x) is VarDecl:
      self.id = x.id
      self.ref = x

  def __str__(self):
    return self.id

class VarDecl:
  def __init__(self, id, t):
    self.id = id
    self.type = t

  def __str__(self):
    return self.id

class AbsExpr(Expr):
  def __init__(self, var, e1):
    self.var = decl(var)
    self.expr = expr(e1)

  def __str__(self):
    return f"\\{self.var}.{self.expr}"

class AppExpr(Expr):
  def __init__(self, e1, e2):
    self.lhs = expr(e1)
    self.rhs = expr(e2)

  def __str__(self):
    return f"({self.lhs} {self.rhs})"

class LambdaExpr(Expr):
  def __init__(self, vars, e1):
    self.vars = list(map(decl, vars))
    self.expr = expr(e1)

  def __str__(self):
    parms = ",".join(str(v) for v in self.vars)
    return f"\\({parms}).{self.expr}"

class CallExpr(Expr):
  def __init__(self, fn, args):
    self.fn = expr(fn)
    self.args = list(map(expr, args))

  def __str__(self):
    args = ",".join(str(a) for a in self.args)
    return f"{self.fn} ({args})"

class PlaceholderExpr(Expr):
  def __str__(self):
    return "_"

def expr(x):
  if type(x) is bool:
    return BoolExpr(x)
  if type(x) is str:
    return IdExpr(x)
  return x

def decl(x):
  if type(x) is str:
    return VarDecl(x)
  return x

def lookup(id, stk):
  for scope in reversed(stk):
    if id in scope:
      return scope[id]
  return None

def resolve(e, stk = []):
  if type(e) is BoolExpr:
    return e

  if type(e) is AndExpr:
    resolve(e.lhs, stk)
    resolve(e.rhs, stk)
    return e 

  if type(e) is OrExpr:
    resolve(e.lhs, stk)
    resolve(e.rhs, stk)
    return e 

  if type(e) is NotExpr:
    resolve(e.expr, stk)
    return e

  if type(e) is IfExpr:
    resolve(e.cond, stk)
    resolve(e.true, stk)
    resolve(e.false, stk)
    return e

  if type(e) is IdExpr:
    decl = lookup(e.id, stk)
    if not decl:
      raise Exception("Name Lookup Error")
    e.ref = decl
    return e

  if type(e) is AbsExpr:
    resolve(e.expr, stk + [{e.var.id : e.var}])
    return e

  if type(e) is AppExpr:
    resolve(e.lhs, stk)
    resolve(e.rhs, stk)
    return e

  if type(e) is LambdaExpr:
    resolve(e.expr, stk + [{var.id : var for var in e.vars}])
    return e

  if type(e) is CallExpr:
    resolve(e.fn, stk)
    for a in e.args:
      resolve(e.fn, stk)
    return e

def is_bool(x):
  if isinstance(x, Type):
    return x == boolType
  if isinstance(x, Expr):
    return is_bool(check(x))

def is_int(x):
  if isinstance(x, Type):
    return x == intType
  if isinstance(x, Expr):
    return is_int(check(x))

def is_same_type(type1, type2):
  if type(type1) is not type(type2):
    return False

  if type(type1) is BoolType:
    return True
  
  if type(type1) is IntType:
    return True

  assert False

def has_same_type(e1, e2):
  return is_same_type(check(e1), check(e2))

def check_bool(e):
  return boolType

def check_int(e):
  return intType

def check_logical_binary(e, op):
  if is_bool(e1) and is_bool(e2):
    return boolType
  
  raise Exception(f"invalid operands to '{op}'")

def check_not(e):
  return check_logical_binary(e, "not")

def check_and(e):
  return check_logical_binary(e, "and")

def check_or(e):
  return check_logical_binary(e, "or")

def check_arithmetic_binary(e, op):
  if is_int(e.lhs) and is_int(e.rhs):
    return intType
  
  raise Exception(f"invalid operands to '{op}'")

def check_add(e):
  return check_arithmetic_binary(e, "+")

def check_sub(e):
  return check_arithmetic_binary(e, "-")

def check_mul(e):
  return check_arithmetic_binary(e, "*")

def check_div(e):
  return check_arithmetic_binary(e, "/")

def check_rem(e):
  return check_arithmetic_binary(e, "%")

def check_relational(e, op):
  if has_same_type(e.lhs, e.rhs):
    return boolType
  
  raise Exception(f"invalid operands to '{op}'")  

def check_eq(e):
  return check_relational(e, "==")

def check_ne(e):
  return check_relational(e, "!=")

def check_lt(e):
  return check_relational(e, "<")

def check_gt(e):
  return check_relational(e, ">")

def check_le(e):
  return check_relational(e, "<=")

def check_ge(e):
  return check_relational(e, ">=")

def check_id(e):
  return e.ref.type

def check_abs(e):
  type1 = e.var.type
  type2 = check(e.expr)
  return ArrowType(type1, type2)

def check_app(e):
  type1 = check(e.lhs)

  if type(type1) is not ArrowType:
    raise Exception("application to non-abstraction")

  if not is_same(type1.parm, type2):
    raise Exception("invalid operand to abstraction")

  return type2

def do_check(e):
  assert isinstance(e, Expr)

  if type(e) is BoolExpr:
    return check_bool(e)

  if type(e) is NotExpr:
    return check_not(e)

  if type(e) is AndExpr:
    return check_and(e)

  if type(e) is OrExpr:
    return check_or(e)

  if type(e) is IfExpr:
    return check_if(e)

  if type(e) is IntExpr:
    return check_int(e)

  if type(e) is AddExpr:
    return check_add(e)

  if type(e) is SubExpr:
    return check_sub(e)

  if type(e) is MulExpr:
    return check_mul(e)

  if type(e) is DivExpr:
    return check_div(e)

  if type(e) is RemExpr:
    return check_rem(e)

  if type(e) is NegExpr:
    return check_neg(e)

  if type(e) is EqExpr:
    return check_eq(e)

  if type(e) is NeExpr:
    return check_ne(e)

  if type(e) is LtExpr:
    return check_lt(e)

  if type(e) is GtExpr:
    return check_gt(e)

  if type(e) is LeExpr:
    return check_le(e)

  if type(e) is GeExpr:
    return check_ge(e)

  assert False

def check(e = {}):
  if not e.type:
    e.type = do_check(e)

  return e.type
def subst(e, s):  
  if type(e) is BoolExpr:
    return e

  if type(e) is AndExpr:
    e1 = subst(e.lhs, s)
    e2 = subst(e.rhs, s)
    return AndExpr(e1, e2)

  if type(e) is NotExpr:
    e1 = subst(e.expr, s)
    return NotExpr(e1)

  if type(e) is OrExpr:
    e1 = subst(e.lhs, s)
    e2 = subst(e.rhs, s)
    return OrExpr(e1, e2)

  if type(e) is IfExpr:
    e1 = subst(e.cond, s)
    e2 = subst(e.true, s)
    e3 = subst(e.false, s)
    return IfExpr(e1, e2, e3)

  if type(e) is IdExpr:
     if e.ref in s:
      return s[e.ref]
    else:
      return e

  if type(e) is AbsExpr:
     e1 = subst(e.expr, s)
    return AbsExpr(e.var, e1)

  if type(e) is AppExpr:
    e1 = subst(e.lhs, s)
    e2 = subst(e.rhs, s)
    return AppExpr(e1, e2)

  if type(e) is LambdaExpr:
    e1 = subst(e.expr, s)
    return LambdaExpr(e.vars, e1)

  if type(e) is CallExpr:
    e0 = subst(e.fn, s)
    args = list(map(lambda x: subst(x, s), e.args))
    return CallExpr(e0, args)

#Implements tuples, variants, types(bool and int)
class Mult_type(Type):
	pass


class Function_type(Mult_type):
	"""Represents types of the form '(T1, T2, ..., Tn) -> T0'."""
	def __init__(self, parms, ret):
		self.parms = parms
		self.ret = ret

	def __str__(self):
		parms = ", ".join(map(str, self.parms))
		return f"({parms}) -> {self.ret})"

	def __repr__(self):
		return f"<func_t|{self}|{address(self)}>"

class Tuple_type(Mult_type):
	def __init__(self, types):
		assert False
	def __str__(self):
		return "{T1, T2, ..., Tn}"
	def __repr__(self):
		return f"<tuple_t|{self}|{address(self)}>"

class Record_type(Mult_type):

	def __init__(self, fields):
		type(fields) is set
		for field in fields:
			assert type(field) is Field_decl
		self.fields = fields

	def __str__(self):
		return "record {f1, f2, ..., fn}"

	def __repr__(self):
		return f"<record_t|{self}|{address(self)}>"

class Variant_type(Mult_type):
	
	def __init__(self, fields):
		type(fields) is set
		for field in fields:
			assert type(field) is Field_decl
		self.fields = fields

	def __str__(self):
		return "variant {f1, f2, ..., fn}"

	def __repr__(self):
		return f"<variant_t|{self}|{address(self)}>"

class Bool_type(Nullary_type):
	def __str__(self):
		return "Bool"

class Int_type(Nullary_type):
	def __str__(self):
		return "Int"


bool_type = Bool_type()
int_type = Int_type()

def is_type_bool(x):
	assert isinstance(x, LangAtom)

	if isinstance(x, Expr):
		return is_type_bool(get_expr_type(x))
	if isinstance(x, Type):
		return x == bool_type
	assert False

def is_type_int(x):
	assert isinstance(x, LangAtom)

	if isinstance(x, Expr):
		return is_type_int(get_expr_type(x))
	if isinstance(x, Type):
		return x == int_type
	assert False
	
def is_same_type(t1, t2):
	assert isinstance(t1, Type)
	assert isinstance(t2, Type)

	if type(t1) is not type(t2):
		return False
	if isinstance(t1, Nullary_type):
		return True
	if type(t1) is Arrow_type:
		return t1.ret == t2.ret and t1.parm == t2.parm
	if type(t1) is Function_type:
		return t1.ret == t2.ret and t1.parms == t2.parms
	assert False

def have_same_type(lhs, rhs):
	assert isinstance(lhs, LangAtom)
	assert isinstance(rhs, LangAtom)

	return is_same_type(get_type(lhs), get_type(rhs))

def get_type(x):
	assert isinstance(x, LangAtom)

	if isinstance(x, Expr):
		return get_expr_type(x)
	if isinstance(x, Decl):
		return get_decl_type(x)
	if isinstance(x, Type):
		return x
		
class UniversalType(Type):
  def __init__(self, ts, t):
    Type.__init__(self)
    self.parms = list(map(type_decl, ts))
    self.type = type_expr(t)

  def __str__(self):
    ts = ",".join([str(p) for p in self.parms])
    return f"∀[{ts}].{str(self.type)}"

class ExistentialType(Type):
  def __init__(self, vs, t):
    Type.__init__(self)
    self.parms = list(map(type_decl, vs))
    self.type = type_expr(t)

  def __str__(self):
    ps = ",".join([str(p) for p in self.parms])
    return f"∃[{ps}].{str(self.type)}"

boolType = BoolType()
intType = IntType()
depType = DepType()

