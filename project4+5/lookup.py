#Paul Camardo
# Theory of Programming Languages

from lang import *

def lookup(id, stack):
  for scope in reversed(stack):
    if id in scope:
      return scope[id]
  return None

def resolve(e, stack = []):
  if type(e) is BoolExpr:
    return e

  if type(e) is NotExpr:
    resolve(e.expr, stack)
    return e

  if type(e) is AndExpr:
    resolve(e.lhs, stack)
    resolve(e.rhs, stack)
    return e 

  if type(e) is OrExpr:
    resolve(e.lhs, stack)
    resolve(e.rhs, stack)
    return e 

  if type(e) is IfExpr:
    resolve(e.cond, stack)
    resolve(e.true, stack)
    resolve(e.false, stack)
    return e

  if type(e) is IdExpr:
    decl = lookup(e.id, stack)
    if not decl:
      raise Exception("Name Lookup Error")
    e.ref = decl
    return e

  if type(e) is AbsExpr:
    resolve(e.expr, stack + [{e.var.id : e.var}])
    return e

  if type(e) is AppExpr:
    resolve(e.lhs, stack)
    resolve(e.rhs, stack)
    return e

  if type(e) is LambdaExpr:
    resolve(e.expr, stack + [{var.id : var for var in e.vars}])
    return e

  if type(e) is CallExpr:
    resolve(e.fn, stack)
    for a in e.args:
      resolve(e.fn, stack)
    return e
