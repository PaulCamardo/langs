from ast import *

# size -- compute the size of an expression
# height -- compute the height of an expression
# same -- Return true if two expressions are identical
# value -- compute the value of an expression
# step -- Return an expression representing a single step of evaluation
# reduce -- Calls step rep
def is_value(e):
	return isinstance(e,Expr)

def is_reducible(e):
	return not is_value(e)

def size(e):
	if is_value(e):
		return 1
	if isinstance(e,BoolExpr):
		return 1 + size(e.NotExpr)
	if isinstance(e,NotExpr):
		return 1 + size(e.NotExpr)
	if isinstance(e, AndExpr):
		return 1 + size(e.lhs) + size(e.rhs)
	if isinstance(e, OrExpr):
		return 1 + size(e.lhs) + size(e.rhs)


def height(e):
	if is_value(e):
		return 1
	if isinstance(e,BoolExpr):
		return 1 + height(e.NotExpr)
	if isinstance(e,NotExpr):
		return 1 + size(e.NotExpr)
	if isinstance(e, AndExpr):
		return 1 + max(height(e.lhs)) + height(e.rhs)
	if isinstance(e, OrExpr):
		return 1 + max(height(e.lhs)) + height(e.rhs)

def same(e1,e2):
	if type(e1) is not type(e2):
		return false
	if is_value(e1):
		return e1 is e2
	if isinstance(e1,BoolExpr):
		retun same(e1.BoolExpr, e2.BoolExpr)
	if isinstance(e1,NotExpr):
		retun same(e1.NotExpr, e2.NotExpr)
	if isinstance(e1,AndExpr):
		return same(e1.lhs, e2.lhs) and same(e1.rhs, e2.rhs)
	if isinstance(e1,OrExpr):
		return same(e1.lhs, e2.lhs) and same(e1.rhs, e2.rhs)

def resolve(e, scope = []):
  	if type(e) is AppExpr:
    	resolve(e.lhs, scope)
    	resolve(e.rhs, scope)
    	return

  	if type(e) is AbsExpr:
    	resolve(e.expr, scope + [e.var])
    	return

  	if type(e) is IdExpr:
    	for var in reversed(scope):
     	 	if e.id == var.id:
        	e.ref = var 
        	return
    	raise Exception("lookup name error")

  	assert False

def subst(e, s):
  	if type(e) is IdExpr:
    	if e.ref in s: 
      	return s[e.ref]
    else:
      	return e

  	if type(e) is AbsExpr:
    	return AbsExpr(e.var, subst(e.expr, s))

  	if type(e) is AppExpr:
    	return AppExpr(subst(e.lhs, s), subst(e.rhs, s))

  	assert False

def step_app(e):
  
  	if is_reducible(e.lhs): 
    	return AppExpr(step(e.lhs), e.rhs)

  	if type(e.lhs) is not AbsExpr:
    	raise Exception("application of non-lambda")

 	if is_reducible(e.rhs): # App-2
    	return AppExpr(e.lhs, step(e.rhs))

 	s = {e.lhs.var: e.rhs}
  	return subst(e.lhs.expr, s);

def step(e):
  	assert isinstance(e, Expr)
  	assert is_reducible(e)

  	if type(e) is AppExpr:
    	return step_app(e)

 	 assert False


def step(e):
	assert is_reducible(e)
	if type(e) is not Expr
	if type(e.Expr) is BoolExpr
		if e.expr.value == true:
			return BoolExpr(False)
		else: 
			return BoolExpr(True)
	if is_value(e.Expr)
		return Not step(e.expr) 
	#NotExpr	
	assert  is_value(e.expr) 
	if type(e.Expr) is NotExpr
		if e.expr.value == e.value:
			return NotExpr(e.value)
		else: 
			return NotExpr(e.value)
	#AndExpr
	assert is_value(e.lhs) and is_value(e.rhs)
	if type(e.Expr) is AndExpr
		if is_reducible(e.lhs)
			return AndExpr(step(e.lhs),e.rhs)
		else: 
			return AndExpr(e.lhs,step(e.rhs))
	#OrExpr
	assert is_value(e.lhs) and is_value(e.rhs)
	if type(e.Expr) is OrExpr
		if is_reducible(e.lhs)
			return OrExpr(step(e.lhs),e.rhs)
		else: 
			return OrExpr(e.lhs,step(e.rhs))
	assert isinstance(e, Expr)
  	assert is_reducible(e)

  	if type(e) is AppExpr:
    	return step_app(e)

  	assert False


def reduce(e):
	print(e)
	while is_reducible(e)
		e = step(e)
		print(e)
	return e












